package azry.com.shared;

import java.io.Serializable;

public class UserModel  implements Serializable{


    int id;
    public String name;
    public String role ;
    public String language;


    public UserModel(int id, String name, String role, String language) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.language = language;
    }

    public UserModel() {

    }

    public String getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
