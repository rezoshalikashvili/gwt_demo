package azry.com.shared;


 import azry.com.server.Person;

import java.util.List;

public interface PersonServiceInterface {
    String getName(int id);
    List<Person> getPersons();

    void save(Person person);

    boolean update(UserModel person);

    boolean delete(UserModel person);

}
