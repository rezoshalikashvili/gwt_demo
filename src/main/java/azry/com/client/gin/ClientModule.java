package azry.com.client.gin;

import azry.com.client.application.ApplicationModule;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;
import com.gwtplatform.mvp.shared.proxy.RouteTokenFormatter;

import static azry.com.client.places.NameTokens.LOGIN;


public class ClientModule  extends AbstractPresenterModule {
    protected void configure() {
        install(new DefaultModule.Builder()
                .tokenFormatter(RouteTokenFormatter.class)
                .defaultPlace(LOGIN)
                .errorPlace(LOGIN)
                .unauthorizedPlace(LOGIN)
                .build());

        install(new ApplicationModule());
    }
}
