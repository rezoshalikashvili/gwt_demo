
package azry.com.client.application.home.persons;

import azry.com.shared.UserModel;
import com.google.gwt.dom.client.Style;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.*;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.editing.GridEditing;
import com.sencha.gxt.widget.core.client.grid.editing.GridInlineEditing;

import java.util.ArrayList;
import java.util.List;

import static azry.com.client.application.home.persons.PersonPresenter.*;

public class PersonView extends ViewWithUiHandlers<PersonUiHandlers> implements PersonPresenter.MyView {
    interface Binder extends UiBinder<Widget, PersonView> {
    }

    @UiField
    ResizeLayoutPanel panel;

    @Inject
    PersonView(Binder binder) {
        initWidget(binder.createAndBindUi(this));
        ContentPanel grid = createGrid();
        TextButton b = new TextButton("Get Users  ", new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {

            }

        });


        ContentPanel p = new ContentPanel();
        p.setButtonAlign(BoxLayoutContainer.BoxLayoutPack.CENTER);
        p.addButton(b);
        b.setWidth(90);

        DockLayoutPanel widgets = new DockLayoutPanel(Style.Unit.EM);

        widgets.addSouth(b,4);
        widgets.add(grid );
        panel.add(widgets);

    }

    private TextButton getSaveButton() {
        return new TextButton("Save", new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                getUiHandlers().onPersonSave();

            }
        });
    }

    private TextButton getDeleteButton(final CheckBoxSelectionModel<UserModel> model) {

        return new TextButton("Delete selected users", new SelectEvent.SelectHandler() {
            public void onSelect(SelectEvent event) {
                getUiHandlers().onDelete(model);
            }
        });
    }


    private ContentPanel createGrid() {
        listStore = new ListStore<UserModel>(gridProperties.id());

        ColumnConfig<UserModel, String> nameCol = new ColumnConfig<UserModel, String>(gridProperties.name());
        ColumnConfig<UserModel, String> roleCol = new ColumnConfig<UserModel, String>(gridProperties.role() );
        ColumnConfig<UserModel, String> langCol = new ColumnConfig<UserModel, String>(gridProperties.language());
        languages.add("JAVA");
        languages.add("C");


        IdentityValueProvider<UserModel> valueProvider = new IdentityValueProvider<UserModel>();

        CheckBoxSelectionModel<UserModel> checkBoxSelectionModel = new CheckBoxSelectionModel<UserModel>(valueProvider);

        checkBoxSelectionModel.setSelectionMode(com.sencha.gxt.core.client.Style.SelectionMode.MULTI);
        List<ColumnConfig<UserModel, ?>> columns = new ArrayList<ColumnConfig<UserModel, ?>>();
        columns.add(checkBoxSelectionModel.getColumn());
        columns.add(nameCol);
        columns.add(roleCol);


        ComboBoxCell<String> comboBoxCell = new ComboBoxCell<String>(languages, new LabelProvider<String>() {
            public String getLabel(String item) {
                return item;
            }
        });


        comboBoxCell.setTriggerAction(ComboBoxCell.TriggerAction.ALL);

        langCol.setCell(comboBoxCell);
        langCol.setWidth(200);

        columns.add(langCol);

        ColumnModel<UserModel> columnModel = new ColumnModel<UserModel>(columns);

        GridView<UserModel> gridView = new GridView<UserModel>();
        gridView.setAutoExpandColumn(nameCol);

        final Grid<UserModel> grid = new Grid<UserModel>(listStore, columnModel, gridView);

        grid.setSelectionModel(checkBoxSelectionModel);
        final GridEditing<UserModel> gridEditing = new GridInlineEditing<UserModel>(grid);

        gridEditing.addEditor(nameCol, new TextField());
        gridEditing.addEditor(roleCol, new TextField());

        ContentPanel panel = new ContentPanel();

        panel.setButtonAlign(BoxLayoutContainer.BoxLayoutPack.CENTER);
        panel.setHeadingText("User management");
        panel.add(grid);
        TextButton save = getSaveButton();
        TextButton deleteButton = getDeleteButton(checkBoxSelectionModel);
        panel.addButton(save);
        panel.addButton(deleteButton);





        return panel;
    }
}
