package azry.com.client.application.home.shared;

import azry.com.shared.UserModel;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

public class AddUserEvent extends GwtEvent<AddUserEvent.Handler> {

    private UserModel userModel;

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    public AddUserEvent(UserModel userModel) {

        this.userModel = userModel;
    }
    public static void fire(HasHandlers source, UserModel userModel) {
        source.fireEvent(new AddUserEvent(userModel));
    }
    @Override
    protected void dispatch(Handler handler) {
        handler.onAddUser( this);
    }

    public interface  Handler extends EventHandler{
        void onAddUser(AddUserEvent addUserEvent);
    }

    public static final Type<Handler> TYPE = new Type<Handler>();

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }
}
