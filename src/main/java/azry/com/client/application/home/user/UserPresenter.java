
package azry.com.client.application.home.user; 

import azry.com.client.application.home.shared.AddUserEvent;
import azry.com.shared.UserModel;


import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;

public class UserPresenter extends PresenterWidget<UserPresenter.MyView> implements UserUiHandlers {
    private PersonServiceAsync personServiceAsync;

    public void onSubmit(String userName, String userRole, String language) {


        AddUserEvent.fire(this,new UserModel(-1,userName,userRole,language));
    }

    interface MyView extends View, HasUiHandlers<UserUiHandlers> {
    }


    @Inject
    UserPresenter(EventBus eventBus, MyView view, PersonServiceAsync personServiceAsync) {
        super(eventBus, view);
        this.personServiceAsync = personServiceAsync;

        getView().setUiHandlers(this);
    }

}
