
package azry.com.client.application.home.user;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class UserModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindSingletonPresenterWidget(UserPresenter.class, UserPresenter.MyView.class, UserView.class);
    }
}
