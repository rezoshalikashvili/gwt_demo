package azry.com.client.application.home.persons;

import azry.com.shared.UserModel;
import com.gwtplatform.mvp.client.UiHandlers;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;

interface PersonUiHandlers extends UiHandlers {

    public void onPersonSave();

    void onDelete(CheckBoxSelectionModel<UserModel> model);
}
