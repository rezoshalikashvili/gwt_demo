
package azry.com.client.application.home.user;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;

public class UserView extends ViewWithUiHandlers<UserUiHandlers> implements UserPresenter.MyView {
    interface Binder extends UiBinder<Widget, UserView> {
    }

    @Inject
    UserView(Binder binder) {
        languages.add("JAVA");
        languages.add("C");
        Widget root = binder.createAndBindUi(this);
        FramedPanel panel = new FramedPanel();
        panel.setHeadingText("ADD USER");
        panel.add(root);
        initWidget(panel);

        submit.setHeight(30);
        submit.setWidth(70);
    }


    @UiField
    TextField user_name;

    @UiField
    TextButton submit;

    @UiField
    TextField user_role;

    @UiField(provided = true)
    public ListStore<String> languages = new ListStore<String>(new ModelKeyProvider<String>() {
        public String getKey(String item) {
            return item;
        }
    });

    @UiField(provided = true)
    public LabelProvider<String> v = new LabelProvider<String>() {
        public String getLabel(String item) {
            return item;
        }
    };

    @UiField
    ComboBox combobox;



    public TextField getUserName() {
        return user_name;
    }

    public TextButton getButton() {
        return submit;
    }

    public TextField getUserrole() {
        return user_role;
    }

    public ComboBox getCombobox() {
        return combobox;
    }

    @UiHandler("submit")
    public void submitSelect(SelectEvent event) {
        getUiHandlers().onSubmit(user_name.getText(),user_name.getText(),combobox.getText());
    }

}
