package azry.com.client.application.home;

import azry.com.client.application.home.persons.PersonModule;
import azry.com.client.application.home.user.UserModule;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class HomeModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        install(new PersonModule());
        install(new UserModule());

        bindPresenter(HomePresenter.class, HomePresenter.MyView.class, HomeView.class, HomePresenter.MyProxy.class);
    }
}
