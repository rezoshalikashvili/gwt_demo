package azry.com.client.application.home.user;

import azry.com.shared.UserModel;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface PersonServiceAsync {
   void getUsers(AsyncCallback<List<UserModel>> async);

   void save(UserModel model, AsyncCallback<Void> async);

   void update(List<UserModel> models, AsyncCallback<Integer> async);

   void delete(List<UserModel> models, AsyncCallback<Void> async);
}
