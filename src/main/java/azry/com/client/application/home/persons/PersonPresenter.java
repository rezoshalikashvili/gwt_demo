
package azry.com.client.application.home.persons; 

import azry.com.client.application.home.shared.AddUserEvent;
import azry.com.client.application.home.user.PersonServiceAsync;
import azry.com.shared.UserModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.data.shared.Store;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PersonPresenter extends PresenterWidget<PersonPresenter.MyView> implements PersonUiHandlers, AddUserEvent.Handler {

    private PersonServiceAsync personServiceAsync;

    public  static ListStore<UserModel> listStore;

    @Override
    public void onAddUser(AddUserEvent addUserEvent) {
        UserModel userModel = addUserEvent.getUserModel();
        personServiceAsync.save(userModel, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("fail");
            }

            @Override
            public void onSuccess(Void result) {
                Window.alert("success");
            }
        });
    }


    public interface GridProperties extends PropertyAccess<UserModel> {
        ModelKeyProvider<UserModel> id();
        ValueProvider<UserModel, String> name();
        ValueProvider<UserModel, String> language();
        ValueProvider<UserModel, String> role();
    }
    public static GridProperties gridProperties = GWT.create(GridProperties.class);


    public static ListStore<String> languages = new ListStore<String>(new ModelKeyProvider<String>() {
        public String getKey(String item) {
            return item;
        }
    });



    public void onPersonSave() {
        final List<UserModel> changed = getChangedModels();
        final int size = changed.size();
        personServiceAsync.update(changed, new AsyncCallback<Integer>() {
            public void onFailure(Throwable caught) {
                Window.alert("can't save");
            }

            public void onSuccess(Integer result) {
                if (size != result) {
                    Window.alert("saved only " + size + " entries");
                }

            }
        });
    }


    private List<UserModel>   getChangedModels() {
        List<UserModel> changed = new ArrayList<UserModel>();
        Collection<Store<UserModel>.Record> modifiedRecords = listStore.getModifiedRecords();
        for (Store<UserModel>.Record model : modifiedRecords){
            Collection<Store.Change<UserModel, ?>> changes = model.getChanges();
            for(Store.Change<UserModel,?> change: changes){
                change.modify(model.getModel());
            }
            changed.add(model.getModel());
        }
        return changed;
    }

    public void onDelete( CheckBoxSelectionModel<UserModel> model) {
        personServiceAsync.delete(model.getSelectedItems(), new AsyncCallback<Void>() {
            public void onFailure(Throwable caught) {
                Window.alert("can't delete");
            }

            public void onSuccess(Void result) {

            }
        });
    }

    interface MyView extends View, HasUiHandlers<PersonUiHandlers> {
    }

    @Inject
    PersonPresenter(EventBus eventBus, MyView view, PersonServiceAsync personServiceAsync) {
        super(eventBus, view);
        this.personServiceAsync = personServiceAsync;
        addVisibleHandler(AddUserEvent.TYPE,this);
        getView().setUiHandlers(this);
    }




}
