
package azry.com.client.application.home.persons;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

public class PersonModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindSingletonPresenterWidget(PersonPresenter.class, PersonPresenter.MyView.class, PersonView.class);
    }
}
