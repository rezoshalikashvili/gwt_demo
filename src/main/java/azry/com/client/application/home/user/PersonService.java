package azry.com.client.application.home.user;

import azry.com.shared.UserModel;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("person")
public interface PersonService extends RemoteService {

     List<UserModel> getUsers();

     void  save(UserModel model);

      Integer update(List<UserModel> models);

      void delete(List<UserModel> models);
}
