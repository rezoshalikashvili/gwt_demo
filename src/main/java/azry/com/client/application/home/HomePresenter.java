package azry.com.client.application.home;

import azry.com.client.application.home.persons.PersonPresenter;
import azry.com.client.application.home.user.PersonService;
import azry.com.client.application.home.user.PersonServiceAsync;
import azry.com.client.application.home.user.UserPresenter;
import azry.com.shared.UserModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.DefaultGatekeeper;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.Slot;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import java.util.List;

import static azry.com.client.application.ApplicationPresenter.SLOT_APPLICATION;
import static azry.com.client.application.home.persons.PersonPresenter.listStore;
import static azry.com.client.places.NameTokens.HOME;

public class HomePresenter extends Presenter<HomePresenter.MyView, HomePresenter.MyProxy> implements HomeUiHandlers {
    interface MyView extends View, HasUiHandlers<HomeUiHandlers> {
    }



    private PersonServiceAsync personServiceAsync = (PersonServiceAsync) GWT.create(PersonService.class);



    public void requestUsers() {
        personServiceAsync.getUsers(new AsyncCallback<List<UserModel>>() {
            public void onFailure(Throwable caught) {
                Window.alert("fail");
            }
            public void onSuccess(List<UserModel> result) {
                listStore.replaceAll(result);
            }
        });
    }






    @NameToken(HOME)
    @ProxyCodeSplit
    public interface MyProxy extends ProxyPlace<HomePresenter> {
    }

    public static final Slot SLOT_HOME = new Slot();
    public static final Slot SLOT_SIDE = new Slot();

    @Inject
    HomePresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy,
            UserPresenter userPresenter,
            PersonPresenter personPresenter) {
        super(eventBus, view, proxy, SLOT_APPLICATION);
        setInSlot(SLOT_HOME,personPresenter);
        setInSlot(SLOT_SIDE,userPresenter);
        getView().setUiHandlers(this);
    }

}
