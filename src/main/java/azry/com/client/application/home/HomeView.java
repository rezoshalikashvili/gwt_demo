package azry.com.client.application.home;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;


import javax.inject.Inject;

import static azry.com.client.application.home.HomePresenter.SLOT_SIDE;


public class HomeView extends ViewWithUiHandlers<HomeUiHandlers> implements HomePresenter.MyView {

    @UiField
    DockLayoutPanel rootDock;
    @UiField
    ResizeLayoutPanel userForm;
    @UiField
    ResizeLayoutPanel personTable;

    interface Binder extends UiBinder<Widget, HomeView> {
    }

    @Inject
    HomeView( Binder binder  ) {
        initWidget(binder.createAndBindUi(this));

        bindSlot(HomePresenter.SLOT_HOME, personTable);
        bindSlot(SLOT_SIDE,userForm);

    }








}
