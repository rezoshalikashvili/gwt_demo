package azry.com.client.application.home.user;

import com.gwtplatform.mvp.client.UiHandlers;

interface UserUiHandlers extends UiHandlers {
    void onSubmit(String userName, String userRole,String language);
}
