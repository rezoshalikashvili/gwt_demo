package azry.com.client.application.login;

import com.gwtplatform.mvp.client.UiHandlers;

interface LoginUiHandlers extends UiHandlers {
    void onLogin(String text, String text1);
}
