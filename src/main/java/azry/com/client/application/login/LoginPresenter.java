package azry.com.client.application.login;

import azry.com.client.application.CurrentUser;
import azry.com.client.places.NameTokens;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.presenter.slots.Slot;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;

import static azry.com.client.application.ApplicationPresenter.SLOT_APPLICATION;

public class LoginPresenter extends Presenter<LoginPresenter.MyView, LoginPresenter.MyProxy> implements LoginUiHandlers {


    public static final Slot SLOT_LOGIN = new Slot();
    private PlaceManager placeManager;

    @Override
    public void onLogin(String name, String password) {

        boolean loggedIn = !(name.equals("") || password.equals(""));
        CurrentUser.getInstance().setLoggedIn(loggedIn);

        PlaceRequest placeRequest = new PlaceRequest.Builder()
                .nameToken(NameTokens.HOME)
                .build();
        placeManager.revealPlace(placeRequest);
    }

    interface MyView extends View, HasUiHandlers<LoginUiHandlers> {
    }

    @NoGatekeeper
    @NameToken(NameTokens.LOGIN)
    @ProxyCodeSplit
    interface MyProxy extends ProxyPlace<LoginPresenter> {
    }


    @Inject
    LoginPresenter(
            EventBus eventBus,
            MyView view,
            MyProxy proxy,
            PlaceManager placeManager) {
        super(eventBus, view, proxy,SLOT_APPLICATION);
        this.placeManager = placeManager;

        getView().setUiHandlers(this);
    }

}
