package azry.com.client.application;

import com.gwtplatform.mvp.client.annotations.DefaultGatekeeper;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;

@DefaultGatekeeper
public class LoginGateKeeper implements Gatekeeper {
    @Override
    public boolean canReveal() {
       return CurrentUser.getInstance().isLoggedIn();
    }
}
