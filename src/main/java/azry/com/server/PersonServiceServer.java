package azry.com.server;




import azry.com.shared.PersonServiceInterface;
import azry.com.shared.UserModel;

import javax.ejb.Singleton;

import javax.persistence.*;
import java.util.List;

@Singleton
public class PersonServiceServer implements PersonServiceInterface {

   @PersistenceContext
    private EntityManager entityManager;

    public String getName(int id) {
        Person person = entityManager.find(Person.class, id);
        if (person != null)
            return person.getName();
        return null;
    }

    public List<Person> getPersons() {
        List<Person> list = entityManager.createQuery("from Person ", Person.class).getResultList();
        if (list != null)
            return list;
        return null;

    }


    public void save(Person person){


        entityManager.persist(person);

    }

    public boolean update(UserModel person) {
        Person found = entityManager.find(Person.class, person.getId());
        if (found!= null){
            found.setName(person.getName());
            found.setRole(person.getRole());
            found.setLanguage(person.getLanguage());
            return true;
        }
        return false;
    }

    public boolean delete(UserModel person) {

        Person found = entityManager.find(Person.class,person.getId());
        if (found!= null){
            entityManager.remove(found);
            return true;
        }
        return false;
    }
}
