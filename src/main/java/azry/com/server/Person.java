package azry.com.server;


import javax.persistence.*;

@Entity
@Table
public class Person {


    public Person() {
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    private String name ;
    private String role  = "user";
    private String language;


    public Person(String name, String role, String language) {
        this.name = name;
        this.role = role;
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
