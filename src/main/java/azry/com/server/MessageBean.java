package azry.com.server;

import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;


@MessageDriven(name = "MessageEJB")
public class MessageBean implements MessageListener {
    public MessageBean() {
    }

    public void onMessage(Message var1) {
        try {
            var1.acknowledge();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
