package azry.com.server;

import azry.com.shared.PersonServiceInterface;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "Demo", urlPatterns = "/demo")
public class
Demo extends HttpServlet {
    @EJB
    PersonServiceInterface bean;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = bean.getName(1);
        System.out.println(name);


    }
}
