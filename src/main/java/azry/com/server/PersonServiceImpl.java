package azry.com.server;


import azry.com.client.application.home.user.PersonService;
import azry.com.shared.PersonServiceInterface;
import azry.com.shared.UserModel;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import javax.ejb.EJB;
import java.util.ArrayList;
import java.util.List;

public class PersonServiceImpl extends RemoteServiceServlet implements PersonService {
    @EJB
    private PersonServiceInterface personService;


    public List<UserModel> getUsers() {
        List<Person> persons = personService.getPersons();
        List<UserModel> ans = new ArrayList<UserModel>();

        for (Person person : persons) {
            ans.add(new UserModel(person.getId(), person.getName(), person.getRole(),person.getLanguage()));
        }
        return ans;
    }

    public void save(UserModel model) {
        personService.save(new Person(model.getName(), model.getRole(),model.getLanguage()));
    }

    public Integer update(List<UserModel> models) {
        Integer ans = 0;
        for (UserModel model : models) {
             if (personService.update(model)) {
                ans++;
            }

        }
        return ans;
    }

    public void delete(List<UserModel> models) {
        for (UserModel model : models){
            personService.delete(model);
        }
    }
}